PREFIX ?=	/usr/local
SCRIPTS=	1 2 3 ctrlaltdel

all:
	$(CC) $(CFLAGS) halt.c -o halt $(LDFLAGS)
	$(CC) $(CFLAGS) pause.c -o pause $(LDFLAGS)
	$(CC) $(CFLAGS) vlogger.c -o vlogger $(LDFLAGS)

install:
	install -d ${DESTDIR}/sbin
	install -m755 halt ${DESTDIR}/sbin
	install -m755 pause ${DESTDIR}/sbin
	install -m755 vlogger ${DESTDIR}/sbin
	install -m755 shutdown ${DESTDIR}/sbin/shutdown
	install -m755 modules-load ${DESTDIR}/sbin/modules-load
	install -m755 zzz ${DESTDIR}/sbin
	ln -sf zzz ${DESTDIR}/sbin/ZZZ
	ln -sf halt ${DESTDIR}/sbin/poweroff
	ln -sf halt ${DESTDIR}/sbin/reboot
	install -d ${DESTDIR}/${PREFIX}/share/man/man1
	install -m644 pause.1 ${DESTDIR}/${PREFIX}/share/man/man1
	install -d ${DESTDIR}/${PREFIX}/share/man/man8
	install -m644 zzz.8 ${DESTDIR}/${PREFIX}/share/man/man8
	install -m644 shutdown.8 ${DESTDIR}/${PREFIX}/share/man/man8
	install -m644 halt.8 ${DESTDIR}/${PREFIX}/share/man/man8
	install -m644 modules-load.8 ${DESTDIR}/${PREFIX}/share/man/man8
	install -m644 vlogger.8 ${DESTDIR}/${PREFIX}/share/man/man8
	ln -sf halt.8 ${DESTDIR}/${PREFIX}/share/man/man8/poweroff.8
	ln -sf halt.8 ${DESTDIR}/${PREFIX}/share/man/man8/reboot.8
	install -d ${DESTDIR}/etc/sv
	install -d ${DESTDIR}/etc/runit/runsvdir
	install -d ${DESTDIR}/etc/runit/core-services
	install -m644 core-services/*.sh ${DESTDIR}/etc/runit/core-services
	install -m755 ${SCRIPTS} ${DESTDIR}/etc/runit
	install -m644 functions $(DESTDIR)/etc/runit
	install -m644 crypt.awk  ${DESTDIR}/etc/runit
	install -m644 rc.conf ${DESTDIR}/etc
	install -m755 rc.local ${DESTDIR}/etc
	install -m755 rc.shutdown ${DESTDIR}/etc
	ln -sf /run/runit/reboot ${DESTDIR}/etc/runit/
	ln -sf /run/runit/stopit ${DESTDIR}/etc/runit/
	cp -R --no-dereference --preserve=mode,links -v runsvdir/* ${DESTDIR}/etc/runit/runsvdir/
	cp -R --no-dereference --preserve=mode,links -v services/* ${DESTDIR}/etc/sv/

clean:
	-rm -f halt pause vlogger

.PHONY: all install clean

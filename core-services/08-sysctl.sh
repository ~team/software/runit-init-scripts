# vim: set ts=4 sw=4 et:

if [ -x /sbin/sysctl ]; then
    msg "Loading sysctl(8) settings..."
    mkdir -p /run/vsysctl.d
    if [[ ! -d /etc/sysctl.d ]]; then
        mkdir -p /etc/sysctl.d
    fi
    if [[ ! -d /usr/local/lib/sysctl.d ]]; then
        mkdir -p /usr/local/lib/sysctl.d
    fi
    if [[ ! -d /usr/lib/sysctl.d ]]; then
        mkdir -p /usr/lib/sysctl.d
    fi
    for i in /run/sysctl.d/*.conf \
        /etc/sysctl.d/*.conf \
        /usr/local/lib/sysctl.d/*.conf \
        /usr/lib/sysctl.d/*.conf; do

        if [[ -d /run/vsysctl.d ]]; then
            if [ -e "$i" ] && [ ! -e "/run/vsysctl.d/${i##*/}" ]; then
                ln -s "$i" "/run/vsysctl.d/${i##*/}"
            fi
        fi
    done
    if [[ -d /run/vsysctl.d ]]; then
        vsysctl_count=$(ls /run/vsysctl.d/*.conf 2>/dev/null | wc -l)
        if [ "$vsysctl_count" != "0" ]; then
            for i in /run/vsysctl.d/*.conf; do
                sysctl -p "$i"
            done
        fi
    fi
    rm -rf -- /run/vsysctl.d
    if [[ -f /etc/sysctl.conf ]]; then
        sysctl -p /etc/sysctl.conf
    fi
fi
